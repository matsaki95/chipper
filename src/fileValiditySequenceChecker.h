#ifndef CHIPPER_FILEVALIDITYCHECKSEQUENCEVECTOR_H
#define CHIPPER_FILEVALIDITYCHECKSEQUENCEVECTOR_H

#include <string>
#include <vector>
#include "printError.h"
#include "getNextWord.h"

class fileValiditySequenceChecker {
private:
    std::vector<std::string> definedSequences;

    bool sequenceExists(const std::string &sequence) {
        for (const auto &definedSequence : definedSequences) {
            if (sequence == definedSequence) {
                return true;
            }
        }
        return false;
    }

public:
    bool definingSequence = false;
    std::string sequenceBeingDefined;

    void checkEndOfSequenceDefinitionValidity(const std::string &line, const std::string &fileName, const int lineCount) {
        if (!definingSequence) {
            printError("Not defining any sequence, yet there is a semicolon", fileName, lineCount, 2);
        }

        int i = 0;
        std::string buffer = getNextWord(line, i);//Skipping over the semicolon
        buffer = getNextWord(line, i);//Getting what's after the semicolon
        if (!buffer.empty()) {
            printError("Invalid characters following semicolon", fileName, lineCount, 2);
        }

        if (!sequenceExists(sequenceBeingDefined)) {
            definedSequences.push_back(sequenceBeingDefined);
        }
        definingSequence = false;
    }

    void checkSequenceLineValidity(const std::string &line, const std::string &fileName, const int lineCount) {
        int i = 0;
        std::string buffer = getNextWord(line, i);
        std::string sequence = buffer;

        //Check to see if we're gonna define a sequence or just call one
        buffer = getNextWord(line, i);
        if (buffer == ":" || sequence[sequence.length() - 1] == ':') {
            if (buffer == ":" && sequence[sequence.length() - 1] == ':') {
                printError("A colon following a colon", 2);
            }
            if (sequence[sequence.length() - 1] == ':') {
                sequence.pop_back();
            }

            if (definingSequence) {
                printError("A different sequence is still being defined", fileName, lineCount, 2);
            }
            //Checking there's nothing after the colon
            buffer = getNextWord(line, i);
            if (!buffer.empty()) {
                printError("Invalid characters following after the colon", fileName, lineCount, 2);
            }
            //Everything is fine, we're now defining this sequence
            definingSequence = true;
            sequenceBeingDefined = sequence;

        } else if (!sequenceExists(sequence)) {
            //If sequence is called while it doesn't exist
            printError("\"" + sequence + "\" is not defined", fileName, lineCount, 2);

        } else if (!buffer.empty() && !isAnInteger(buffer)) {
            //As for the emptiness check, the number is optional and if buffer is empty, isANumber will return false
            printError("\"" + buffer + "\" is not a valid repetition amount", fileName, lineCount, 2);

        } else if (!buffer.empty()) {
            //Being here would mean that buffer holds an integer (as a string of course), so we're gonna check if anything follows
            buffer = getNextWord(line, i);
            if (!buffer.empty()) {
                if (buffer.length() == 1) {
                    printError("The character '" + buffer + "' following the amount of repetitions is invalid", fileName, lineCount, 2);
                } else {
                    printError("The characters \"" + buffer + "\" following the amount of repetitions are invalid", fileName, lineCount, 2);
                }
            }
        }
    }
};

#endif //CHIPPER_FILEVALIDITYCHECKSEQUENCEVECTOR_H
