#include <iostream>

bool isANumber(const char charInQuestion) {
    return '0' <= charInQuestion && charInQuestion <= '9';
}

bool isANumber(const std::string &stringInQuestion) {
    if (stringInQuestion.empty()) {
        return false;
    }

    if (!isANumber(stringInQuestion[0])) {
        return false;//Numbers don't start with dots. Starting the following loop from the start of the string would mean it could allow a dot to at the start of the number.
    }

    bool dotPresent = false;
    for (int i = 1; i < stringInQuestion.length(); i++) {
        if (!isANumber(stringInQuestion[i])) {
            if (stringInQuestion[i] == '.') {
                if (dotPresent) {
                    return false;//What kind of number is like 4.8454.58.2.69?
                } else {
                    dotPresent = true;
                }
            } else {
                return false;
            }
        }
    }

    return true;
}

bool isAnInteger(const std::string &stringInQuestion) {
    if (stringInQuestion.empty()) {
        return false;
    }

    for (char i : stringInQuestion) {
        if (!isANumber(i)) {
            return false;
        }
    }

    return true;
}
