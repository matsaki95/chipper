#ifndef CHIPPER_PROGRAMOPTIONS_H
#define CHIPPER_PROGRAMOPTIONS_H

#include <vector>
#include <string>
#include <getopt.h>

struct ProgramOptions {
    std::string programCall;//The command that was used to call the program
    std::vector<std::string> filesToPlay{};
    int verbosity = 0;//Level incremented using `-v`
    bool simulationMode = false;//Whether to call SoX or run in a simulation kinda mode
    double bpm = 60;
    bool bpmSetFromProgramArguments = false;
    double multiplier = 1;
};
ProgramOptions programOptions;

const char *shortOptions = "vhsb:m:";

const option longOptions[] = {
        {"verbose",    no_argument,       nullptr, 'v'},
        {"help",       no_argument,       nullptr, 'h'},
        {"simulation", no_argument,       nullptr, 's'},
        {"bpm",        required_argument, nullptr, 'b'},
        {"multiplier", required_argument, nullptr, 'm'},
        {nullptr,      no_argument,       nullptr, 0}
};

#endif //CHIPPER_PROGRAMOPTIONS_H
