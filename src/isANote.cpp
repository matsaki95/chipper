#include <string>
#include "isANumber.h"

bool isANote(const std::string &stringInQuestion) {
    //This check is done here in order to avoid duplicate code in the switch case
    if (stringInQuestion.empty()) {
        return false;
    }
    if (!('A' <= stringInQuestion[0] && stringInQuestion[0] <= 'G')) {
        return false;
    }

    switch (stringInQuestion.length()) {
        case 2:
            if (!isANumber(stringInQuestion[1])) {
                return false;
            }
            break;

        case 3:
            if (stringInQuestion[1] != '#' || !isANumber(stringInQuestion[2])) {
                return false;
            }
            break;

        default:
            return false;
    }
    return true;
}
