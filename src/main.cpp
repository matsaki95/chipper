#include <iostream>
#include <vector>
#include "ProgramOptions.h"
#include "parseProgramOptions.h"
#include "checkFileValidity.h"
#include "playFile.h"


int main(int argc, char *argv[]) {
    //Argument checking
    parseProgramOptions(argc, argv);

    //File check before any operations
    for (auto const &file : programOptions.filesToPlay) {
        checkFileValidity(file);
    }

    for (const auto &fileToPlay: programOptions.filesToPlay) {
        playFile(fileToPlay);
    }

    return 0;
}
