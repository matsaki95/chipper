#include <string>
#include <chrono>
#include <thread>
#include "getNextWord.h"
#include "stepsToSeconds.h"

void pause(const std::string &line) {
    int i = 0;
    getNextWord(line, i);//Skipping over 'P'

    std::string buffer = getNextWord(line, i);
    if (programOptions.verbosity > 0) {
        std::cout << "Pausing for " << stepsToSeconds(buffer) << " seconds." << std::endl;
    }
    if (!programOptions.simulationMode) {
        std::this_thread::sleep_for(stepsToMilliseconds(buffer));
    }
}
