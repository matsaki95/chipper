#include <string>
#include "getNextWord.h"
#include "isANumber.h"

bool isAValidBPM(const std::string &stringInQuestion) {
    if (!isANumber(stringInQuestion)) {
        return false;
    }

    double asANumber = std::stod(stringInQuestion);
    return asANumber > 0;
}
