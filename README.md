# Chipper

Chipper is a command line program that basically reads a file with notes and durations in it (in a specified format) and outputs them through your speakers as chiptune sounds.

_The only dependency is SoX (specifically the play command, which is used to generate sound)._

_Chipper has been designed to run on Linux, but all you should have to do to run it on Windows is add `#include <system.h>` at the start of `main.cpp` before compiling it._

### Input File Format

To have Chipper play any majestic chiptune music you need to feed it an input file (`chipper FILE`) which contains your magic notes and is formatted in the following way.

##### Time

At the start of the file there's a mandatory value you need to specify that is the BPM (Beats Per Minute).
That determines how much time there is between beats.

The time between those beats is split into 4 steps.
In Chipper, we use steps to specify the duration.

Mathematically, we define the BPM as `bpm = beats / minutes`. From there we can turn the minutes to seconds `bpm = beats / (seconds / 60)`. Since a beat is 4 steps, we also get `bpm = (steps / 4) / (seconds / 60)`. A few steps later we arrive to the equation `seconds = (steps * 15) / bpm`, which is how Chipper figures out how long to pause and how long should it tell SoX to play the notes for.

If, for some reason, you want to use seconds instead of steps to specify duration, you can do it, in a way, by specifying the BPM as 15.

##### Basic note

For a basic note you first write the note then the octave (from 0 to 9) and then the duration of said note.

Optionally, you can also add a `&` to the end of the line to specify that you don't want chipper to wait for the note to finish, but instead go right ahead to the next one.

_Examples:_
- `A4 2` will play the note A from the 4th octave for 2 steps.
- `G#2 4 &` will play the note G# from the 2nd octave for 4 steps and will cause the program to go to the next file of the input file without waiting for this note to finish.

##### Pause
The pause is pretty basic and simple: You just write P and then the duration of the pause.

_Examples:_
- `P 2` will pause for 2 steps.

##### Sequence

When there are parts that are repeated over and over again, it is annoying to have to write those parts again and again every time they are required. This is where sequences come in.

A sequence is, as the name implies, a sequence of notes and pauses (and also other sequences) under a certain name that you can later call in your file to have the sequence played like normal notes and pauses.

Here's an example to get us kickstarted:

```
120

opening1:
C5  1
A4  1
E4  1
A4  1
;

opening2:
C5  1
G4  1
E4  1
G4  1
;

opening3:
B4  1
G#4 1
E4  1
G#4 1
;

opening :
opening1 2
opening2 2
opening3 4
;

opening
```

There are a few things we can conclude from the example:
- To start defining a sequence, we write its name followed by a colon `:`.
It doesn't matter if it's separated from the name with a space or not.
- Whatever follows that line until a line with just a semicolon `;` is part of the sequence.
- We can have (already defined) sequences inside other sequences.
- We can optionally add a number following the sequence name which says how many times should the sequence be repeated.

### Full file examples

##### A sample from _Offenbach_'s  _Barcallore_
```
100

circleFinish2 :
F#4 2 &
B4  2
F#4 4 &
B4  4

F#4 2 &
A4  2
;

intro :
F#4 4 &
A4  4
circleFinish2
;

circle :
F#3 4
G3  2
G3  4

F#3 2
F#3 2
E3  2
G3  2
G3  4

F#3 2
F#3 2
E3  2
G3  2
G3  4
F#3 2
F#3 4
;

circleFinish :
F#3 2 &
B3  2
F#3 4 &
B3  4
F#3 2 &
A3  2
;

start :
circle
circleFinish
;

intro 2
start 2

circle
circleFinish2



secondCircle :
A3  4
B3  2
B3  4

A3  2 &
C4  2
A3  4 &
C4  4

B3  2 &
D4  2
B3  4 &
D4  4

A3  2 &
C#4 2
A3  4 &
C#4 4

G3  2 &
B3  2
G3  4 &
B3  4

F#3 2 &
A3  2
F#3 4 &
A3  4

circleFinish2
;

secondCircle 2
```

##### The intro of _Toby Fox_'s _Asgore_
```
105

P 8

main :
F4 2
F4 2
A4 2
A4 2
G4 6
;

mainFollowUp :
P 2

E4 2
E4 2
G4 2
G4 2
F4 6

P 2

D4 2
D4 2
F4 2
F4 2
E4 2
E4 2
C4 2
C4 2
D4 2
D4 2
B3 2
B3 2
A3 6
;


main
mainFollowUp

P 4

main
P 2
C5 2
C5 2
G4 2
G4 2
A4 6
P 2
A4 2
G4 1
A4 3
C5 2
D5 6

F5 1
F5 1
F5 2
E5 1
D5 1
C5 2
E5 2
D5 6
```

##### The damn _Tetris melody_ (the sequence names and whatnot came from [a lovely song based on the melody](https://youtu.be/hWTFG3J1CP8))
```
150

IAmTheMan :
E5 4
B4 2
C5 2
D5 4
;

whoArrangesTheBlocks :
C5 2
B4 2
A4 2
A4 4
C5 2
E5 4
;

upAbove :
E5 4
C5 4
A4 4
A4 6
;

thatDescendUponMe :
D5 2
C5 2
B4 2
B4 4
C5 2
D5 4

upAbove
;

theyComeDown :
D5 4
F5 2
A5 4
;

andISpinThemAround :
G5 2
F5 2
E5 2
E5 4
C5 2
E5 4
;

tillTheyFitInTheGround :
D5 2
C5 2
B4 2
B4 4
C5 2
D5 4
upAbove
;



IAmTheMan
whoArrangesTheBlocks
thatDescendUponMe
P 4
theyComeDown
andISpinThemAround
thatDescendUponMe

P 4

andTheLines :
D5 2
C5 2
B4 6
C5 2
D5 4
upAbove
;

thatIHaveMisjudgedIt :
G5 2
F5 2
E5 2
E5 4
F5 2
E5 3
D5 3
C5 2
B4 3
B4 3
C5 2
D5 4
upAbove
;

IAmTheMan
whoArrangesTheBlocks
andTheLines
P 4
theyComeDown
thatIHaveMisjudgedIt

P 4

canIHave :
E5 8
C5 8
D5 8
B4 8
C5 8
A4 8
G#4 12
;

whyMust :
E5 8
C5 8
D5 8
B4 8
C5 4
D5 4
A5 8
G#5 12
;

canIHave
P 4
whyMust
```
