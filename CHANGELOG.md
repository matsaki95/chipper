# Chipper changelog

Every merge to master indicates a new version

You want to report a bug or want to suggest a feature? Open an issue at <https://www.gitlab.com/matsaki95/chipper>


### Version 2.1 "Repeated manipulation"

- _Repeating notes._
You can add a number after the duration and before the ampersand (if it exists) to specify how many times you want the note to be repeated.
- _BPM manipulation options._
Two new options are available `-b`/`--bpm` and `-m`/`--multiplier`.
The former sets a specific bpm for all files and the latter multiplies all the bpm values used (even the one supplied with `-b`) by the number supplied.
- _Fixed bugs???_
I probably fixed stuff, I can't remember.
I know, I should be more active, but I have my final year of high school to worry about.
_Gotta get all those exam points to get to the institute I want, man!_



### Version 2 "The proper time for a revamp sequence"

A ton of new goodies!!!
A whole new revolution!
Sequences!
Proper use of beats and steps!
So much I forgot!

Here's a list that goes through whatever I can remember while going though the commits:
- _The code is structured in a much much better way_ and though it still has a lot of room for improvement, it's gone a good way from before.
- _Moved all code files to their own folder in the repository._
Should keep things cleaner.
- _Proper time definition._
No more playing around with buzzwords.
The length of beats is defined by the bpm and each beat is divided to 4 steps.
We use steps now for defining the duration of things, like we should've done from the very start (I didn't know back then, sorry).
- _Compatibility with Windows._
All it takes to compile the program on Windows is to add a `#include <system.h>` at the start of `main.cpp`.
It works great, but there seems to be a delay before a note gets played (probably Windows taking its time to load SoX).
- _Better program argument handling._
We are now using getopt to assist in the task of handling the arguments you pass to the program.
This helps better comply with standards and whatnot and carries some error checks with it as well, it's great.
- _More program arguments._
Assisted by getopt, it's super easy to just add more arguments (and that's what's been done here).
Currently, in addition to `help`, we have `verbose` (which adds up the more times you pass it for more and more verbose output) and `simulation` (which just tells Chipper to never call SoX and never pause).
- _Sequences._
Great stuff, like functions in programming, let's you reuse parts of your song without writing them again and again.
- _Error messages._
Those have been revisited.
Should be a bit more helpful.
The real improvement in them though is bellow the hood, given that now they are just an overloaded function that outputs the error message in a format depending on what other arguments you passed it.
- _Multiple input files._
No longer do you have to execute multiple commands to have multiple files play.
Now Chipper plays every single file that is passed to it in the program arguments (in the order they are passed (it used to be all at once but the idea was abandoned because of future plans for sequences)).
- _Proper usage message._
More standard compliant and more helpful.
Everyone wins.



### Version 1 "I'll chip your ears off!"

The first version of Chipper is finally ready!

To use chipper you need to call it from the command line and pass it the file you want it to read (instructions on how the file should be formatted in the [readme](https://gitlab.com/matsaki95/chipper/blob/master/README.md)).
Chipper will then first go through the file, checking it for errors, and then interpret it, translating it to sox commands, thus producing (hopefully) music out of the notes in the file.



### Version 0.0.2 "What is it you do?"

Added a readme with some basic information.



### Version 0.0.1 "Licence to pun"

Added a license, or rather, an unlicence.
